import api from '../../api';

export default {
    namespaced: true,
    state() {
        return {
            currentUser: null,
            requestedProfile: null,
            isValidResetToken: false
        };
    },
    getters: {
        currentUser: ({ currentUser }) => currentUser,
        isLoggedIn: ({ currentUser }) => currentUser !== null,
        requestedProfile: ({ requestedProfile }) => requestedProfile,
        isValidResetToken: ({ isValidResetToken }) => isValidResetToken
    },
    actions: {
        loadUser({ commit }) {
            return api.getCurrentUser()
            .then((user) => {
                commit('setUser', user);
                return true;
            })
            .catch(() => {
                return false;
            });
        },

        getUserById({ commit, dispatch }, id) {
            return api.getUserById(id)
            .then((user) => {
                commit('setRequestedProfile', user);
                return true;
            })
            .catch((err) => {
                dispatch('apiError', err, { root: true });
                return false;
            });
        },

        getUserByInvite({ dispatch }, { email, hash }) {
            return api.getUserByInvite(email, hash)
            .then(({ name }) => {
                return { name, email };
            })
            .catch((err) => {
                dispatch('apiError', err, { root: true });
                throw new err;
            });
        },

        auth({ commit }, { email, password }) {
            return api.login(email, password)
            .then((user) => {
                commit('setUser', user);
                return true;
            })
            .catch(() => {
                return false;
            });
        },

        register({ commit, dispatch }, { name, email, password, hash = '' }) {
            return api.register(name, email, password, hash)
            .then((user) => {
                commit('setUser', user);
                return true;
            })
            .catch((err) => {
                dispatch('apiError', err, { root: true });
            });
        },

        invite({ dispatch }, { name, email }) {
            return api.invite(name, email)
            .then(({ inviteLink }) => {
                return inviteLink;
            })
            .catch((err) => {
                dispatch('apiError', err, { root: true });
                throw err;
            });
        },

        sendPasswordResetMail(store, { email }) {
            return api.sendPasswordResetMail(email)
            .then(() => true)
            .catch(() => false);
        },

        updatePassword(store, { email, hash, password }) {
            return api.updatePassword(email, hash, password)
            .then(() => true)
            .catch(() => false);
        },

        logout({ commit }) {
            commit('setUser', null);
            return true;
        }
    },
    mutations: {
        setUser(state, user) {
            state.currentUser = user;
        },

        setRequestedProfile(state, user) {
            state.requestedProfile = user;
        },

        validateResetToken(state, value) {
            state.isValidResetToken = Boolean(value);
        }
    }
};
