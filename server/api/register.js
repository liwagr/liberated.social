const express = require('express');
const bodyParser = require('body-parser');
const createHttpError = require('http-errors');
const { getToken } = require('../middleware/auth');

module.exports = (config, dal) => {
    const app = express();
    const { secret } = config.auth;

    app.use(bodyParser.json());

    app.post('/', (req, res, next) => {
        const { name, email, password, hash } = req.body;

        Promise.resolve().then(() => {
            if (hash) {
                return dal.users.createNewFriend(name, email, password, hash);
            } else {
                return dal.users.createNewUser(name, email, password);
            }
        })
        .then((user) => {
            const { id } = user;

            res.cookie('auth', {
                id,
                token: getToken(id, secret)
            });
            res.send({ id, name });
        })
        .catch(() => {
            return next(createHttpError(401));
        });
    });

    return app;
};
