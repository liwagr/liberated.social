const EasyPg = require('@rolandliwag/easypg');
const UserTables = require('./users');
const PostTables = require('./posts');
const CommentsTable = require('./comments');

module.exports = class DAL {
    constructor(connString) {
        this.db = new EasyPg(connString);
        this.users = new UserTables(this.db);
        this.posts = new PostTables(this.db);
        this.comments = new CommentsTable(this.db);
    }
};
