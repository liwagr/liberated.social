const express = require('express');
const bodyParser = require('body-parser');
const createHttpError = require('http-errors');

module.exports = (config, dal) => {
    const app = express();

    app.use(bodyParser.json());

    app.post('/', (req, res, next) => {
        const { email, hash, password } = req.body;

        if (hash !== 'abcd') {
            return next(createHttpError(412));
        }

        dal.users.getUserByEmail(email)
        .catch(() => {
            throw new Error('NotFound');
        })
        .then(({ id }) => {
            return dal.users.updatePassword(id, password);
        })
        .then(() => {
            res.send({});
        })
        .catch((err) => {
            if (err.message === 'NotFound') {
                return next(createHttpError(400));
            }
            next(createHttpError(500));
        });
    });

    return app;
};
