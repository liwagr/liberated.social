const crypto = require('crypto');

const getHash = (password, salt) => {
    return new Promise((resolve, reject) => {
        crypto.pbkdf2(password, salt, 100000, 64, 'sha512', (err, key) => {
            if (err) {
                return reject(err);
            }

            resolve(key.toString('hex'));
        });
    });
};

const getSalt = () => {
    let salt = '';
    
    for (let i = 0; i < 64; i++) {
        salt += String.fromCharCode(Math.floor(Math.random() * (127 - 32)) + 32);
    }

    return salt;
};

module.exports = class UserTables {
    constructor(db) {
        this.db = db;
    }

    authenticate(email, password) {
        return this.db.query({
            text: 'SELECT id, name, salt, password_hash FROM users WHERE email=$1',
            params: [email]
        })
        .then((result) => {
            if (!result || !result.rows.length) {
                throw new Error('NotFound');
            }

            const { id, name, salt, password_hash: hash } = result.rows[0];

            return getHash(password, salt)
            .then((key) => {
                if (key !== hash) {
                    throw new Error('Mismatch');
                }
                return { id, name };
            });
        });
    }

    createNewUser(name, email, password) {
        const salt = getSalt();

        return getHash(password, salt)
        .then((hash) => this.db.query({
            text: `INSERT INTO users (name, email, password_hash, salt)
                    VALUES ($1, $2, $3, $4) RETURNING id, name`,
            params: [name, email, hash, salt]
        }))
        .then((result) => {
            if (!result.rows || !result.rows.length) {
                throw new Error('NotAdded');
            }
            return result.rows[0];
        });
    }

    createNewFriend(name, email, password, inviteHash) {
        const salt = getSalt();

        let userId;

        return getHash(password, salt)
        .then((hash) => 
            this.db.runQueriesInTransaction([{
                text: `INSERT INTO users (name, email, password_hash, salt)
                        VALUES ($1, $2, $3, $4) RETURNING id, name`,
                params: [name, email, hash, salt],
                handler: (result) => {
                    if (!result.rows || !result.rows.length) {
                        throw new Error('NotAdded');
                    }

                    userId = result.rows[0].id;
                }
            }, {
                text: `INSERT INTO relationships (user_id, friend_id) 
                        SELECT $1, inviter_id FROM invites WHERE email=$2 AND hash=$3`,
                params: () => ([userId, email, inviteHash]),
                handler: () => {}
            }, {
                text: `INSERT INTO relationships (user_id, friend_id)
                        SELECT inviter_id, $1 FROM invites WHERE email=$2 AND hash=$3`,
                params: () => ([userId, email, inviteHash]),
                handler: () => {}
            }])
        )
        .then(() => ({ 
            id: userId, 
            name
        }));
    }

    getUserById(id) {
        return this.db.query({
            text: 'SELECT * FROM users WHERE id=$1',
            params: [id]
        })
        .then((result) => {
            if (!result.rows || !result.rows.length) {
                throw new Error('NotFound');
            }
            
            const { id, name, email } = result.rows[0];
            return { id, name, email };
        });
    }

    getUserByEmail(email) {
        return this.db.query({
            text: 'SELECT * FROM users WHERE email=$1',
            params: [email]
        })
        .then((result) => {
            if (!result.rows || !result.rows.length) {
                throw new Error('NotFound');
            }
            
            const { id, name, email } = result.rows[0];
            return { id, name, email };
        });
    }

    getInviteByEmailHash(email, hash) {
        return this.db.query({
            text: 'SELECT * FROM invites WHERE email=$1 AND hash=$2',
            params: [email, hash]
        })
        .then((result) => {
            if (!result.rows || !result.rows.length) {
                throw new Error('NotFound');
            }
            
            const { name, email } = result.rows[0];
            return { name, email };
        });
    }

    getDetailedUserProfile(id) {
        return this.db.query({
            text: `SELECT users.id, users.name,
                    json_agg(json_build_object('id', friends.id, 'name', friends.name)) AS friends
                    FROM users
                    LEFT OUTER JOIN relationships ON users.id=relationships.user_id
                    LEFT OUTER JOIN users AS friends ON relationships.friend_id=friends.id
                    WHERE users.id=$1 GROUP BY users.id`,
            params: [id]
        })
        .then((result) => {
            if (!result || !result.rows.length) {
                throw new Error('NotFound');
            }

            const { id, name, email, friends } = result.rows[0];
            return { id, name, email, friends };
        });
    }

    invite(name, email, inviterId) {
        return getHash(email, Buffer.from(Math.random().toString(), 'utf8'))
        .then((hash) => {
            return this.db.query({
                text: `INSERT INTO invites (inviter_id, name, email, hash)
                        VALUES ($1, $2, $3, $4) RETURNING hash`,
                params: [inviterId, name, email, hash]
            });
        })
        .then((result) => {
            if (!result.rows || !result.rows.length) {
                throw new Error('NotAdded');
            }
            return result.rows[0].hash;
        });
    }

    updatePassword(id, password) {
        const salt = getSalt();

        return getHash(password, salt)
        .then((hash) => this.db.query({
            text: `UPDATE users SET password_hash=$2, salt=$3
                    WHERE id=$1 RETURNING id`,
            params: [id, hash, salt]
        }))
        .then((result) => {
            if (!result.rows || !result.rows.length) {
                throw new Error('NotUpdated');
            }
            return result.rows[0];
        });
    }
};
