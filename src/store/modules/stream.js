import api from '../../api';

const state = () => {
    return {
        posts: [],
        lastUpdated: Date.now()
    };
};

const getters = {
    posts: ({ posts }) => posts
};

const actions = {
    getLatestPosts({ commit }) {
        api.getStream()
        .then((posts) => {
            commit('setPosts', posts);
        })
        .catch((err) => {
            commit('logError', err, { root: true });
        });
    },

    submitPost({ commit }, content) {
        api.addPost(content)
        .then((posts) => {
            commit('setPosts', posts);
        });
    },

    submitComment({ commit }, { content, postId }) {
        api.addComment(postId, content)
        .then((comments) => {
            commit('setComments', { postId, comments });
        });
    }
};

const mutations = {
    addPost(state, post) {
        state.posts.unshift(post);
        state.lastUpdated = Date.now();
    },

    setPosts(state, posts) {
        state.posts = posts;
        state.lastUpdated = Date.now();
    },

    setComments(state, { postId, comments }) {
        const post = state.posts.find(({ id }) => id === postId);

        if (post) {
            post.comments = comments;
            state.lastUpdated = Date.now();
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
