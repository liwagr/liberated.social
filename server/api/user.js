const express = require('express');
const createHttpError = require('http-errors');

module.exports = (config, dal, smtp) => {
    const app = express();

    app.get('/authenticatedUser', (req, res, next) => {
        dal.users.getUserById(res.locals.authenticatedUser)
        .then((user) => {
            res.send(user);
        })
        .catch(() => {
            next(createHttpError(401));
        });
    })
    .get('/:id', (req, res, next) => {
        const { id } = req.params;

        if (res.locals.authenticatedUser === id) {
            // Privileged fetch - get all user info
            dal.users.getDetailedUserProfile(id)
            .then((detailedProfile) => {
                res.send(detailedProfile);
            })
            .catch(() => {
                next(createHttpError(503));
            });
        } else {
            // Simple fetch
            dal.users.getUserById(id)
            .then((user) => {
                res.send(user);
            })
            .catch(() => {
                next(createHttpError(401));
            });
        }
    })
    .get('/token/:email/:hash', (req, res) => {
        const { email, hash } = req.params;

        res.send({ email, hash });
    })
    .post('/invite', (req, res, next) => {
        const { name, email } = req.body;

        dal.users.invite(name, email, res.locals.authenticatedUser)
        .then((hash) => {
            const inviteLink = `http://localhost:8080/#/register/${email}/${hash}`;
            const message = {
                to: email,
                from: config.smtp.from,
                subject: 'Join free.social for free',
                text: 'liberated.social is the paid social network.\n' +
                        `Click here to join: ${inviteLink}`
            };

            return smtp.sendMail(message)
            .then(() => res.send({ inviteLink }));
        })
        .catch(() => {
            next(createHttpError(412));
        });
    });

    return app;
};
