const express = require('express');
const bodyParser = require('body-parser');

module.exports = (config, dal, smtp) => {
    const app = express();

    app.use(bodyParser.json());

    app.post('/', (req, res) => {
        const { email } = req.body;

        dal.users.getUserByEmail(email)
        .then((user) => {
            const token = 'abcdef';
            const message = {
                to: email,
                from: config.smtp.from,
                subject: 'Password reset',
                text: `Hi ${user.name},\n` +
                        'Someone tried to reset your password.\n' +
                        `Click here to reset: http://localhost:8080/#/resetPassword/${email}/${token}`
            };

            return smtp.sendMail(message);
        })
        .then(() => res.send({}))
        .catch(() => {
            // Don't let frontend know if user doesn't exist
            res.send({});
        });
    });

    return app;
};
