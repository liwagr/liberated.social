export default {
    state() {
        return {
            errors: []
        };
    },
    getters: {},
    actions: {
        apiError({ commit }, error) {
            commit('logError', error);
        }
    },
    mutations: {
        logError(state, error) {
            state.errors.push(error);
        }
    }
};
