module.exports = {
    auth: {
        secret: '123456'
    },
    db: {
        connString: 'postgresql://postgres:123456@postgres/db'
    },
    smtp: {
        host: 'smtp.example.com',
        port: 465,
        from: 'robot@example.com'
    }
};
