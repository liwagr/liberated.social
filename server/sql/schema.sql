CREATE EXTENSION IF NOT EXISTS citext;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE users (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(), 
    email CITEXT NOT NULL UNIQUE,
    name varchar(255), 
    password_hash varchar(255), 
    salt varchar(255),
    invites_remaining INTEGER NOT NULL DEFAULT 0
);
CREATE INDEX ON users (email);

CREATE TABLE posts (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    author_id uuid NOT NULL CONSTRAINT fk_author_id REFERENCES users(id),
    created_time TIMESTAMP NOT NULL DEFAULT now(),
    content TEXT NOT NULL
);
CREATE INDEX ON posts (author_id);

CREATE TABLE relationships (
    user_id uuid NOT NULL,
    friend_id uuid NOT NULL,
    created_time TIMESTAMP NOT NULL DEFAULT now(),
    PRIMARY KEY (user_id, friend_id)
);

CREATE TABLE invites (
    inviter_id uuid NOT NULL CONSTRAINT fk_user_id REFERENCES users(id),
    name varchar(255) NOT NULL,
    email CITEXT NOT NULL,
    created_time TIMESTAMP NOT NULL DEFAULT now(),
    hash varchar(255) NOT NULL UNIQUE,
    PRIMARY KEY (inviter_id, email)
);

CREATE TABLE comments (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    author_id uuid NOT NULL CONSTRAINT fk_author_id REFERENCES users(id),
    post_id uuid NOT NULL CONSTRAINT fk_post_id REFERENCES posts(id),
    comment_id uuid,
    created_time TIMESTAMP NOT NULL DEFAULT now(),
    content TEXT NOT NULL
);
CREATE INDEX ON comments (author_id);
CREATE INDEX ON comments (post_id);
