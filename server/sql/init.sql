DROP DATABASE IF EXISTS liberated_social;
DROP ROLE IF EXISTS liberated_social;

CREATE ROLE liberated_social;
CREATE DATABASE liberated_social OWNER liberated_social;

GRANT ALL ON DATABASE liberated_social TO liberated_social;
