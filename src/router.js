import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from './components/Home.vue';
import Profile from './components/Profile.vue';
import Login from './components/Login.vue';
import Register from './components/Register.vue';
import Logout from './components/Logout.vue';
import ForgotPassword from './components/ForgotPassword.vue';
import Invite from './components/Invite.vue';
import ResetPassword from './components/ResetPassword.vue';

Vue.use(VueRouter);

export default () => {
    const routes = [{
        path: '/',
        name: 'home',
        component: Home 
    }, { 
        path: '/profile/:userId', 
        name: 'profile',
        props: true,
        component: Profile
    }, {
        path: '/login',
        name: 'login',
        component: Login
    }, {
        path: '/register/:invitedEmail?/:hash?',
        name: 'register',
        props: true,
        component: Register
    }, {
        path: '/logout',
        name: 'logout',
        component: Logout
    }, {
        path: '/forgotPassword',
        name: 'forgotPassword',
        component: ForgotPassword
    }, {
        path: '/invite',
        name: 'invite',
        component: Invite
    }, {
        path: '/resetPassword/:email/:hash',
        name: 'resetPassword',
        props: true,
        component: ResetPassword
    }];

    const router = new VueRouter({ routes });

    return router;
};
