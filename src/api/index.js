const handleResponse = ((response) => {
    return response.json()
    .then((body) => {
        if (body.error) {
            throw new Error(body.error.message);
        }
        return body;
    });
});

const get = (url, query = {}) => {
    const queryString = Object.keys(query).map((param) => {
        return encodeURIComponent(param) + '=' + encodeURIComponent(query[param]);
    }).join('&');

    return fetch(url + (queryString ? `?${queryString}` : ''), {
        headers: {
            'Accept': 'application/json'
        },
        credentials: 'include'
    })
    .then(handleResponse);
};

const post = (url, data = {}) => {
    return fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        credentials: 'include',
        body: JSON.stringify(data)
    })
    .then(handleResponse);
};

export default {
    getStream() {
        return get('/api/stream');
    },

    addPost(content) {
        return post('/api/stream', { content });
    },

    addComment(postId, content) {
        return post(`/api/stream/${postId}/comments`, { content });
    },

    getCurrentUser() {
        return get('/api/user/authenticatedUser');
    },

    getUserById(id) {
        return get(`/api/user/${id}`);
    },

    getUserByResetToken(email, token) {
        return get(`/api/user/token/${email}/${token}`);
    },

    getUserByInvite(email, hash) {
        return get(`/api/invite/${email}/${hash}`);
    },

    login(email, password) {
        return post('/api/auth', { email, password });
    },

    register(name, email, password, hash) {
        return post('/api/register', { name, email, password, hash });
    },

    invite(name, email) {
        return post('/api/user/invite', { name, email });
    },

    sendPasswordResetMail(email) {
        return post('/api/sendPasswordResetMail', { email });
    },

    updatePassword(email, hash, password) {
        return post('/api/updatePassword', { email, hash, password });
    }
};
