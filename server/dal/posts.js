module.exports = class PostTables {
    constructor(db) {
        this.db = db;
    }

    addPost(content, authorId) {
        return this.db.query({
            text: 'INSERT INTO posts (author_id, content) VALUES ($1, $2) RETURNING *',
            params: [authorId, content]
        })
        .then((result) => {
            if (!result.rows.length) {
                throw new Error('NotAdded');
            }
            return result.rows[0];
        });
    }

    getPostsFromFriends(userId) {
        return this.db.query({
            text: `WITH stream AS (
                SELECT posts.*, users.name AS author FROM posts
                LEFT JOIN relationships ON posts.author_id=relationships.friend_id
                LEFT JOIN users ON posts.author_id=users.id
                WHERE relationships.user_id=$1
                UNION
                SELECT posts.*, users.name AS author FROM posts
                LEFT JOIN users ON posts.author_id=users.id
                WHERE author_id=$1
            )
            SELECT stream.*, c.comments
            FROM stream
            LEFT JOIN LATERAL (
                SELECT array_agg(json_build_object('id', id, 'content', content)) AS comments
                FROM comments WHERE stream.id=comments.post_id
            ) AS c ON TRUE
            ORDER BY stream.created_time DESC`,
            params: [userId]
        })
        .then((result) => result.rows.map(({
            id,
            created_time,
            content,
            author,
            author_id,
            comments
        }) =>
            ({
                id,
                author,
                authorId: author_id,
                createdTime: created_time,
                content,
                comments: comments || []
            })
        ));
    }
};
