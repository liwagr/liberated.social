const nodemailer = require('nodemailer');
const Promise = require('bluebird');

module.exports = class SMTP {
    constructor(config) {
        this._smtp = nodemailer.createTransport(config);
    }

    sendMail(opts) {
        return new Promise((resolve, reject) => {
            this._smtp.sendMail(opts, (err, status) => {
                if (err) {
                    return reject(err);
                }
                resolve(status);
            });
        });
    }
};
