module.exports = {
    configureWebpack: {
        devtool: 'source-map'
    },
    devServer: {
        port: 8080,
        watchOptions: {
            aggregateTimeout: 300,
            poll: 1000
        },
        proxy: {
            '/api': {
                target: 'http://localhost:3000'
            },
            '/auth': {
                target: 'http://localhost:3000'
            }
        }
    }
};
