const express = require('express');
const morgan = require('morgan');

const auth = require('./middleware/auth');

const api = require('./api');
const register = require('./api/register');
const sendPasswordResetMail = require('./api/sendPasswordResetMail');
const updatePassword = require('./api/updatePassword');
const invite = require('./api/invite');
const config = require('./config');
const DAL = require('./dal');
const SMTP = require('./smtp');

const app = express();
const dal = new DAL(config.db.connString);
const smtp = new SMTP(config.smtp);

app.use(morgan('dev'));

app.use('/api', express()
    .use('/register', register(config, dal, smtp))
    .use('/sendPasswordResetMail', sendPasswordResetMail(config, dal, smtp))
    .use('/updatePassword', updatePassword(config, dal, smtp))
    .use('/invite', invite(config, dal))
    .use(auth(config, dal))
    .use(api(config, dal, smtp))
);

app.use((error, req, res) => {
    res.status(error.status).send({ error });
});

app.listen(3000);
