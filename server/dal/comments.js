module.exports = class CommentsTables {
    constructor(db) {
        this.db = db;
    }

    getCommentsByPostId(postId) {
        return this.db.query({
            text: 'SELECT * FROM comments WHERE post_id=$1 ORDER BY created_time DESC',
            params: [postId]
        })
        .then((result) => {
            if (!result.rows || result.rows.length === 0) {
                throw new Error('NotFound');
            }

            return result.rows;
        });
    }

    addComment(postId, authorId, comment) {
        return this.db.query({
            text: `INSERT INTO comments (post_id, author_id, content)
                    VALUES ($1, $2, $3) RETURNING id`,
            params: [postId, authorId, comment]
        })
        .then((result) => {
            if (!result.rows || result.rows.length === 0) {
                throw new Error('NotAdded');
            }
        });
    }
};
