# Starting up the Docker containers

    1. Start up the postgres container:
      docker run -p 5432:5432 --name postgres -e POSTGRES_PASSWORD=123456 -d -v /path/to/db/data:/var/lib/postgresql/data --rm postgres:local
    2. Start up the app container:
      docker run -it -v /path/to/app/root/:/app -p 8080:8080 --link postgres:postgres --name app app:latest bash