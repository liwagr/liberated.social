import api from '../../api';

export default {
    state() {
        return {
            loggedIn: false
        };
    },
    getters: {},
    actions: {
        isSessionValid({ commit }) {
            api.validateAuth()
            .then((auth) => {
                commit('setAuth', auth);
            });
        }
    },
    mutations: {
        setAuth(state, auth) {
            state.loggedIn = auth.loggedIn;
        }
    }
};
