const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const createHttpError = require('http-errors');
const crypto = require('crypto');

const auth = (config, dal) => {
    const app = express();
    const { auth: { secret } } = config; 

    app.use(bodyParser.json());
    app.use(cookieParser());

    app.post('/auth', (req, res, next) => {
        const { email, password } = req.body;

        dal.users.authenticate(email, password)
        .then((user) => {
            const { id } = user;

            res.cookie('auth', {
                id,
                token: auth.getToken(id, secret)
            });
            res.locals.authenticatedUser = id;
            res.send(user);
        })
        .catch(() => {
            next(createHttpError(401));
        });
    });
    app.use((req, res, next) => {
        if (!req.cookies.auth && !res.locals.authenticatedUser) {
            return next(createHttpError(401));
        }

        const { id, token } = req.cookies.auth;

        if (auth.getToken(id, secret) !== token) {
            res.cookie('auth', false);
            return next(createHttpError(401));
        }

        res.locals.authenticatedUser = id;
        return next();
    });

    return app;
};

auth.getToken = (id, secret) => {
    const hash = crypto.createHash('sha512');
    hash.update(id + secret);
    return hash.digest('hex');
};

module.exports = auth;
