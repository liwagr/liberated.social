const express = require('express');
const createHttpError = require('http-errors');

module.exports = (config, dal) => {
    const app = express();

    app.get('/:email/:hash', (req, res, next) => {
        const { email, hash } = req.params;
        dal.users.getInviteByEmailHash(email, hash)
        .then(({ name }) => {
            res.send({ name, email });
        })
        .catch(() => {
            next(createHttpError(404));
        });
    });

    return app;
};
