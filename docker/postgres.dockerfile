FROM postgres:10.3

RUN apt-get update
RUN apt-get install -y \
    postgresql-contrib

EXPOSE 5432 5432
