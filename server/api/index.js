const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

module.exports = (config, dal, smtp) => {
    const app = express();

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(cookieParser());

    app.get('/stream', (req, res, next) => {
        dal.posts.getPostsFromFriends(res.locals.authenticatedUser)
        .then((posts) => res.send(posts))
        .catch(next);
    });

    app.post('/stream', (req, res, next) => {
        const content = req.body.content;

        dal.posts.addPost(content, res.locals.authenticatedUser)
        .then(() => dal.posts.getPostsFromFriends(res.locals.authenticatedUser))
        .then((posts) => res.send(posts))
        .catch(next);
    });

    app.post('/stream/:postId/comments', (req, res, next) => {
        const { postId } = req.params;
        const { content } = req.body;

        dal.comments.addComment(postId, res.locals.authenticatedUser, content)
        .then(() => dal.comments.getCommentsByPostId(postId))
        .then((comments) => res.send(comments))
        .catch(next);
    });

    app.use('/user', require('./user')(config, dal, smtp));

    return app;
};
