import Vue from 'vue';
import Vuex from 'vuex';

import errors from './modules/errors';
import stream from './modules/stream';
import user from './modules/user';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        errors,
        stream,
        user
    }
});
